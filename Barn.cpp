#include "Barn.h"
#include <stdlib.h>
#include <iostream>

Barn::Barn()
{
  m_food = 2;
  m_health = true;
}

bool Barn::NextStep()
{ 
	m_food -= 1;
	
	if(m_food < 1)
	{ 

		return false; 
	} 
	
	return true; 
} 

bool Barn::addFood(int userAnswer)
{ 
	m_food += userAnswer;

	if(m_food > 25)
	{ 
		m_food-=userAnswer;
		return false; 
	} 

	return true; 
}   

void Barn::showStatus()
{
	std::cout <<"\nHay in the barn:"<< m_food << " package(s)\n";

	if(m_food<1)
	{
		m_health = false;
	}

	if(m_health == false)
	{
		std::cout<<"\nAnimal is dead. You should have fed it!\n";
	}
	else
	{
		std::cout<<"\nCow is well\n";
	}
}

void Barn::writeDataInFile(FILE* id)
{
	int* pointer = &m_food;

	fwrite(pointer,sizeof(int),1,id);

	fclose(id);
}

void Barn::readDataFromFile(FILE* id)
{
	int* pointer = &m_food;

	fread(pointer,sizeof(int),1, id);

	fclose(id);
}

