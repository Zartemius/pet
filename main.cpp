#include <stdlib.h>
#include "ColouredBarn.h"


enum MenuMethods
{
	NULLS,
	ADD_FOOD,
	NOTHING_TO_DO,
	CHOOSE_CAGE,
	EXIT
};


int main(int argc, char** argv)
{	
	ColouredBarn white_cow;
	ColouredBarn black_cow;
	ColouredBarn animal[] = {white_cow, black_cow};
	FILE* fileId = fopen("/home/artem/Programming/lessons/homework/animal/food.txt","r");

	bool workingLoop = true;
	int choiceOfAnimal = 0;
	int userAnswer = 0;
	int choice = 0;
	int colour = 0;

	std::cout<<"\nWelcome to the Barn.\n";

	while(workingLoop)
	{
		std::cout<<"\nChoose an animal:";
		std::cout<<"\n0.Left Barn";
		std::cout<<"\n1.Right Barn";
		std::cout<<"\n2.Exit game";
		std::cout<< "\nYour choice:";
		std::cin >> choiceOfAnimal;

		//animal[choiceOfAnimal].readDataFromFile(fileId);

		if(choiceOfAnimal == 2)
		{
			break;
		}

		if(choiceOfAnimal > 2)
		{
			std::cout <<"\nYou can enter a number only from the range of options. Try again\n";
		}
		else
		{
			for(int i = 0; i < 2; ++i)
			{
				animal[i].NextStep();
			}
			animal[choiceOfAnimal].showStatus();
			animal[choiceOfAnimal].showColourOfBarn();

			std::cout<<"\nMenu:\n";
			std::cout<< ADD_FOOD << ".Add some hay for the cow\n";
			std::cout<< NOTHING_TO_DO << ".Nothing to do\n";
			std::cout<< CHOOSE_CAGE << ".Choose colour of cage for a cow\n";
			std::cout<< EXIT << ".Exit the game\n";
			std::cout<<"\nYour choice:";
			std::cin>> choice;

			switch(choice)
			{

				case ADD_FOOD:

				std::cout<<"\nHow much hay you want to put:";
				std::cin>> userAnswer;

				if( !animal[choiceOfAnimal].addFood(userAnswer) )
				{
					std::cout<<"\nThere is too much hay in the barn\n";
				}

				break;

				case NOTHING_TO_DO:

				break;

				case CHOOSE_CAGE:

				std::cout <<"\nPlease choose colour of barn\n";
				std::cout << "1.Red\n";
				std::cout << "2.Green\n";
				std::cout << "3.Blue\n";
				std::cout << "4.Your choice:";
				std::cin >> colour;

				animal[choiceOfAnimal].setColourOfBarn(colour);

				break;

				case EXIT:
				for( int i = 0; i < 2; ++i)
				{
					animal[i].writeDataInFile(fileId);
				}
				break;
			}
		}

	}

	return 0;
}
