#ifndef PET_H
#define PET_H
#include <iostream>
#include <cstdio>


class Barn
{
public:
	Barn();
	bool NextStep();
	bool addFood(int userAnswer);
	void showStatus();
	void showHay();
	void writeDataInFile(FILE* id);
	void readDataFromFile(FILE* id);


private:
	int m_food;
	bool m_health;
    
};


#endif
