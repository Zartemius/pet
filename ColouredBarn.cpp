#include "ColouredBarn.h"

ColouredBarn::ColouredBarn()
{
	m_color = 0;
}


void ColouredBarn::setColourOfBarn(int colour)
{
	 m_color = colour;

	 showColourOfBarn();

}

void ColouredBarn::showColourOfBarn()
{
   if(m_color == 1)
   {
	   std::cout << "\nCage is red\n";
   }
   if(m_color == 2)
   {
	   std::cout << "\nCage is green\n";
   }
   if(m_color == 3)
   {
	   std::cout << "\nCage is blue\n";
   }
   if (m_color > 3 || m_color == 0)
   {
	   std::cout <<"\nYou haven't set colour for the barn yet\n";
   }
}
