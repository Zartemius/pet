#ifndef COLOURED_BARN
#define COLOURED_BARN

#include "Barn.h"

class ColouredBarn: public Barn
{

public:
	ColouredBarn();
	void setColourOfBarn(int colour);
	void showColourOfBarn();
private:
	int m_color;

};

#endif
